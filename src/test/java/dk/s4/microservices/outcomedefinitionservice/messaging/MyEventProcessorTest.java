package dk.s4.microservices.outcomedefinitionservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.outcomedefinitionservice.TestFhirServerConfigR4;
import org.hl7.fhir.r4.model.Enumerations.PublicationStatus;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestFhirServerConfigR4.class})
public class MyEventProcessorTest {

    @Autowired
    @Qualifier("myQuestionnaireDaoR4")
    private IFhirResourceDao<Questionnaire> qDao;

    private MyEventProcessor eventProcessor;
    private Identifier identifier;
    private Questionnaire newQuestionnaire;
    private Message message;
    private FhirContext context;
    private IFhirResourceDao<ObservationDefinition> mockObsDefDao;
    private IFhirResourceDao<Task> mockTaskDao;
    private final Topic createTopic = new Topic().setOperation(Operation.Create);
    private final Topic updateTopic = new Topic().setOperation(Operation.Update);

    @Before
    public void setup() throws IOException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("service.env", variables);

        //Set standard Identifier
        identifier = new Identifier().setSystem(System.getenv("OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM")).setValue("value");

        mockObsDefDao = mock(IFhirResourceDao.class);
        mockTaskDao = mock(IFhirResourceDao.class);

        message = mock(Message.class);
        when(message.getBodyType()).thenReturn("Questionnaire");
        when(message.getBody()).thenReturn("");
    }

    private void setQuestionnaire(PublicationStatus status, Identifier identifier) {
        //Set questionnaire that we want to post/put
        newQuestionnaire = new Questionnaire();
        newQuestionnaire.setVersion("abc");
        newQuestionnaire.setStatus(status);
        newQuestionnaire.addIdentifier(identifier);

        //mock parser to return newQuestionnaire
        IParser parser = mock(IParser.class);
        when(parser.parseResource(anyString())).thenReturn(newQuestionnaire);
        context = mock(FhirContext.class);
        when(context.newJsonParser()).thenReturn(parser);
    }

    private Questionnaire aQuestionnaire(PublicationStatus status) {
        Questionnaire newQuestionnaire = new Questionnaire();
        newQuestionnaire.setVersion("abc");
        newQuestionnaire.setStatus(status);
        newQuestionnaire.addIdentifier(identifier);
        return newQuestionnaire;
    }

    @Test
    public void twoActiveWithSameIdAndVersionCollides() {
        setQuestionnaire(PublicationStatus.ACTIVE, identifier);
        qDao.create(newQuestionnaire);

        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Assert.assertTrue(eventProcessor.resourceCollides(createTopic, message));
        qDao.delete(newQuestionnaire.getIdElement());
    }

    @Test
    public void activeQuestionnaireCanOverrideDraft() {
        setQuestionnaire(PublicationStatus.ACTIVE, identifier);

        Questionnaire dbQ = aQuestionnaire(PublicationStatus.DRAFT);
        qDao.create(dbQ);

        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Assert.assertFalse(eventProcessor.resourceCollides(createTopic, message));
        qDao.delete(dbQ.getIdElement());
    }

    @Test
    public void resourceCollidesUpdateHasCollidingResources() {
        setQuestionnaire(PublicationStatus.ACTIVE, identifier);
        qDao.create(newQuestionnaire);

        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Assert.assertTrue(eventProcessor.resourceCollides(updateTopic, message));
        qDao.delete(newQuestionnaire.getIdElement());
    }

    @Test
    public void resourceCollidesUpdateHasNoCollidingResourcesEmptyDb() {
        setQuestionnaire(PublicationStatus.ACTIVE, identifier);

        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Assert.assertFalse(eventProcessor.resourceCollides(updateTopic, message));
    }

    @Test
    public void resourceCollidesUpdateIsDraftEmptyDb() {
        setQuestionnaire(PublicationStatus.DRAFT, identifier);

        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Assert.assertFalse(eventProcessor.resourceCollides(updateTopic, message));
    }

    @Test
    public void resourceCollidesIsNotQuestionnaire() {
        //Initiate eventProcessor with mocks
        eventProcessor = new MyEventProcessor(context, mockObsDefDao, qDao, mockTaskDao);

        Message notQuestionnaireMessage = mock(Message.class);
        when(notQuestionnaireMessage.getBodyType()).thenReturn("NotQuestionnaire");
        when(notQuestionnaireMessage.getBody()).thenReturn("");
        Assert.assertFalse(eventProcessor.resourceCollides(updateTopic, notQuestionnaireMessage));
    }
}