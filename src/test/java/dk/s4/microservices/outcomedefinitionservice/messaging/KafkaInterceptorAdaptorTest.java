package dk.s4.microservices.outcomedefinitionservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.microservicecommon.TestUtils;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class KafkaInterceptorAdaptorTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(KafkaInterceptorAdaptorTest.class);

    private KafkaInterceptorAdaptor interceptorAdaptor;
    private IParser parser;

    @Before
    public void setup() throws IOException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);


        FhirContext context = mock(FhirContext.class);
        interceptorAdaptor = new KafkaInterceptorAdaptor(context);

        parser = mock(IParser.class);
        when(context.newJsonParser()).thenReturn(parser);
    }

    @Test
    public void incomingMessagePreProcessMessageStopsObservation() {
        logger.debug("shouldProcessResourceDoesProcessQuestionnaireResponseTask");
        Task task = new Task().setFocus(new Reference().setType("Observation"));
        Topic topic = new Topic().setDataType(task.fhirType());
        Message message = new Message();

        when(parser.parseResource((String) anyObject())).thenReturn(task);

        assertFalse(interceptorAdaptor.incomingMessagePreProcessMessage(topic,message));

    }

    @Test
    public void incomingMessagePreProcessMessageStopsQuestionnaireResponse() {
        logger.debug("incomingMessagePreProcessMessageDoesNotStopsQuestionnaireResponse");
        Task task = new Task().setFocus(new Reference().setType("QuestionnaireResponse"));
        Topic topic = new Topic().setDataType(task.fhirType());
        Message message = new Message();

        when(parser.parseResource((String) anyObject())).thenReturn(task);

        assertFalse(interceptorAdaptor.incomingMessagePreProcessMessage(topic,message));
    }

    @Test
    public void incomingMessagePreProcessMessageDoesNotStopQuestionnaire() {
        logger.debug("incomingMessagePreProcessMessageStopsQuestionnaire");
        Task task = new Task().setFocus(new Reference().setType("Questionnaire"));
        Topic topic = new Topic().setDataType(task.fhirType());
        Message message = new Message();

        when(parser.parseResource((String) anyObject())).thenReturn(task);

        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(topic,message));
    }
    @Test
    public void incomingMessagePreProcessMessageDoesNotStopObservationDefinition() {
        logger.debug("incomingMessagePreProcessMessageDoesNotStopsObservationDefinition");
        Task task = new Task().setFocus(new Reference().setType("ObservationDefinition"));
        Topic topic = new Topic().setDataType(task.fhirType());
        Message message = new Message();

        when(parser.parseResource((String) anyObject())).thenReturn(task);

        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(topic,message));
    }

    @Test
    public void incomingMessagePreProcessMessageDoesNotStopOtherTopics() {
        logger.debug("incomingMessagePreProcessMessageDoesNotStopsObservationDefinition");
        Observation observation = new Observation();
        Topic topic = new Topic().setDataType(observation.fhirType());
        Message message = new Message();

        when(parser.parseResource((String) anyObject())).thenReturn(observation);

        assertTrue(interceptorAdaptor.incomingMessagePreProcessMessage(topic,message));
    }
}