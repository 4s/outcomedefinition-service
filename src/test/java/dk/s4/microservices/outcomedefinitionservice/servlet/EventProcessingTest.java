package dk.s4.microservices.outcomedefinitionservice.servlet;

import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Message.Prefer;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.outcomedefinitionservice.messaging.MyEventProcessor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Enumerations.PublicationStatus;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.json.simple.parser.ParseException;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EventProcessingTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EventProcessingTest.class);

    private static long FUTURE_TIMEOUT = 5000;
    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;
    private static Server ourServer;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        String filepath = path;
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win"))
            filepath = filepath.replace("file:/", "");
        else
            filepath = filepath.replace("file:", "");

        byte[] encoded = Files.readAllBytes(Paths.get(filepath));
        return new String(encoded, encoding);
    }

    @Test
    public void testCreateQuestionnaire()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Questionnaire.json");
        IParser parser = OutcomeDefinitionService.getServerFhirContext().newJsonParser();
        Questionnaire questionnaire = parser.parseResource(Questionnaire.class, resourceString);
        questionnaire.setStatus(PublicationStatus.DRAFT);

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(parser.encodeResourceToString(questionnaire));
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Questionnaire");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.trace("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("Questionnaire");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test creation of Questionnaire
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("Questionnaire");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Questionnaire", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("1", outputMessage.getEtagVersion());

        assertNotNull(metadata);
    }

    @Test
    public void testUpdateQuestionnaire()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/Questionnaire.json");

        IParser parser = OutcomeDefinitionService.getServerFhirContext().newJsonParser();
        DaoMethodOutcome outcome = OutcomeDefinitionService.getQuestionnaireDao().create(parser.parseResource(Questionnaire.class, resourceString));
        Questionnaire questionnaire = OutcomeDefinitionService.getQuestionnaireDao().read(outcome.getId());
        questionnaire.setName("testUpdateQuestionnaireName");
        questionnaire.setStatus(PublicationStatus.DRAFT); //otherwise update will not be allowed by service

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(parser.encodeResourceToString(questionnaire));
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Questionnaire");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.debug("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Update)
                .setDataCategory(Category.FHIR)
                .setDataType("Questionnaire");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test update of Questionnaire
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataUpdated)
                .setDataCategory(Category.FHIR)
                .setDataType("Questionnaire");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("Questionnaire", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("2", outputMessage.getEtagVersion());

        Questionnaire outputQuestionnaire =  parser.parseResource(Questionnaire.class, message.getBody());
        String name = outputQuestionnaire.getName();
        assertEquals("testUpdateQuestionnaireName", name);

        assertNotNull(metadata);
    }

    @Test
    public void testCreateObservationDefinition()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {

        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/ObservationDefinition.json");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("ObservationDefinition");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.trace("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Create)
                .setDataCategory(Category.FHIR)
                .setDataType("ObservationDefinition");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test creation of ObservationDefinition with code MDC188736
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType("ObservationDefinition");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("ObservationDefinition", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("1", outputMessage.getEtagVersion());

        assertNotNull(metadata);
    }

    @Test
    public void testUpdateObservationDefinition()
            throws IOException, InterruptedException, ExecutionException, TimeoutException, ParseException, MessageParseException {
        //Read resource from file
        final String resourceString = ResourceUtil.stringFromResource("FHIR-clean/ObservationDefinition.json");

        IParser parser = OutcomeDefinitionService.getServerFhirContext().newJsonParser();
        DaoMethodOutcome outcome = OutcomeDefinitionService.getObservationDefinitionDao().create(
                parser.parseResource(ObservationDefinition.class, resourceString));
        ObservationDefinition obsDef = OutcomeDefinitionService.getObservationDefinitionDao().read(outcome.getId());
        obsDef.setPreferredReportName("testUpdateObservationDefinition");

        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(parser.encodeResourceToString(obsDef));
        message.setSender("EventProcessingTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("ObservationDefinition");
        message.setContentVersion(System.getenv("FHIR_VERSION"));

        logger.trace("Sending message to KafkaProducer: " + message.toString());
        //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
        Topic inputTopic = new Topic()
                .setOperation(Operation.Update)
                .setDataCategory(Category.FHIR)
                .setDataType("ObservationDefinition");
        ProducerRecord<String, String> record =
                new ProducerRecord<>(inputTopic.toString(), message.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
        logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());

        //Test update of Observation Definition
        Topic outputTopic = new Topic()
                .setOperation(Operation.DataUpdated)
                .setDataCategory(Category.FHIR)
                .setDataType("ObservationDefinition");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        Message outputMessage = future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
        assertEquals(message.getBodyCategory(), outputMessage.getBodyCategory());
        assertEquals("ObservationDefinition", outputMessage.getBodyType());
        assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
        assertEquals(message.getContentVersion(), outputMessage.getContentVersion());
        assertEquals("2", outputMessage.getEtagVersion());

        ObservationDefinition outputObservationDefinition =  parser.parseResource(ObservationDefinition.class, message.getBody());
        String updatedValidCodedSet = outputObservationDefinition.getPreferredReportName();
        assertEquals("testUpdateObservationDefinition", updatedValidCodedSet );

        assertNotNull(metadata);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        //Use Derby local file based database
        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");

        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = EventProcessingTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        String ENVIRONMENT_FILE = "service.env";
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
        //Disable authentication
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        String ourServerBase = TestUtils.initWebServer(path + "/src/main/webapp/WEB-INF/without-keycloak/web.xml", path + "/target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        mockExternalKafkaProducer = TestUtils.initMockExternalProducer();
        MyEventProcessor eventProcessor = new MyEventProcessor(OutcomeDefinitionService.getServerFhirContext(),
                OutcomeDefinitionService.getObservationDefinitionDao(),
                OutcomeDefinitionService.getQuestionnaireDao(),
                OutcomeDefinitionService.getTaskDao());
        mockKafkaEventProducer = TestUtils.initMockKafkaConsumeAndProcess(OutcomeDefinitionService.getTopics(), eventProcessor);
    }
}
