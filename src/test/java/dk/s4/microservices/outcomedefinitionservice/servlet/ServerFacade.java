package dk.s4.microservices.outcomedefinitionservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import dk.s4.microservices.messaging.EventProcessor;
import org.hl7.fhir.r4.model.*;

import java.util.regex.Pattern;

/**
 * Facade for accessing things on the Service.
 */
public class ServerFacade {
    private ServerFacade() {}

    public static void forceReindexing() {
        OutcomeDefinitionService.forceReindexing();
    }

    public static IFhirResourceDao<ObservationDefinition> getObservationDefinitionDao() {
        return OutcomeDefinitionService.getObservationDefinitionDao();
    }

    public static IFhirResourceDao<Task> getTaskDao() {
        return OutcomeDefinitionService.getTaskDao();
    }

    public static IFhirResourceDao<Questionnaire> getQuestionnaireDao() {
        return OutcomeDefinitionService.getQuestionnaireDao();
    }

    public static FhirContext getServerFhirContext() {
        return OutcomeDefinitionService.getServerFhirContext();
    }
}
