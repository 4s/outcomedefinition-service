package dk.s4.microservices.outcomedefinitionservice.servlet;

import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Message.BodyCategory;
import dk.s4.microservices.messaging.Message.Prefer;
import dk.s4.microservices.messaging.MessageParseException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.outcomedefinitionservice.provider.RestfulFHIRProvidersTest;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Enumerations.PublicationStatus;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Questionnaire;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class MyEventProcessorMockKafkaTest {

    private static final org.slf4j.Logger LOGGER =
        org.slf4j.LoggerFactory.getLogger(MyEventProcessorMockKafkaTest.class);

    private static long FUTURE_TIMEOUT = 15000;

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;
    private static Server ourServer;
    private static EnvironmentVariables environmentVariables;
    private static List<Topic> topics;

    @AfterClass
    public static void afterClass() throws Exception {
        environmentVariables.set("ENABLE_KAFKA", "false");
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        topics =
            Arrays.asList(FhirTopics.create("Questionnaire"), FhirTopics.update("Questionnaire"));
        environmentVariables = new EnvironmentVariables();
        //Use Derby local file based database
        if (System.getenv("DATABASE_TYPE") == null) {
            environmentVariables.set("DATABASE_TYPE", "Derby");
        }

        //Read and set environment variables from .env file:
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        TestUtils.initWebServer("src/main/webapp/WEB-INF/without-keycloak/web.xml", "target/service", environmentVariables, ourPort, ourServer);

        // Instantiate mock Kafka classes
        mockExternalKafkaProducer = TestUtils.initMockExternalProducer();
        mockKafkaEventProducer = TestUtils.initMockKafkaConsumeAndProcess(topics, OutcomeDefinitionService.getEventProcessor());
    }

    private Identifier randomIdentifier() {
        return new Identifier().setSystem(System.getenv("OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM"))
            .setValue(String.valueOf(UUID.randomUUID()));
    }

    private String constructQuestionnaireString(String version, PublicationStatus status,
        Identifier identifier) {
        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setVersion(version);
        questionnaire.setStatus(status);
        questionnaire.addIdentifier(identifier);
        return OutcomeDefinitionService.getServerFhirContext().newJsonParser()
            .encodeResourceToString(questionnaire);
    }

    private Message constructQuestionnaireMessage(String resourceString) {
        Message message = new Message();
        message.setPrefer(Prefer.REPRESENTATION);
        message.setBody(resourceString);
        message.setSender("MyEventProcessorMockKafkaTest");
        message.setCorrelationId(MessagingUtils.verifyOrCreateId(null));
        message.setTransactionId(UUID.randomUUID().toString());
        message.setBodyCategory(BodyCategory.FHIR);
        message.setBodyType("Questionnaire");
        message.setContentVersion(System.getenv("FHIR_VERSION"));
        return message;
    }

    private void sendMessage(Message message, Operation operation)
        throws InterruptedException, ExecutionException, TimeoutException {
        Topic inputTopic = new Topic().setOperation(operation).setDataCategory(Category.FHIR)
            .setDataType("Questionnaire");

        ProducerRecord<String, String> record =
            new ProducerRecord<>(inputTopic.toString(), message.toString());

        LOGGER.trace("Sending message to KafkaProducer: " + message.toString());
        RecordMetadata metadata =
            mockExternalKafkaProducer.send(record).get(FUTURE_TIMEOUT * 2, TimeUnit.MILLISECONDS);
        LOGGER.debug("Message sent to KafkaProducer: offset = " + metadata.offset());
        TimeUnit.SECONDS.sleep(2);
    }

    private Message receiveMessage(Operation operation)
        throws ParseException, MessageParseException, InterruptedException, ExecutionException,
        TimeoutException {
        Topic outputTopic = new Topic().setOperation(operation).setDataCategory(Category.FHIR)
            .setDataType("Questionnaire");

        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);

        return future.get(FUTURE_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private void makeAssertions(Message message, Message outputMessage, String bodyType) {
        Assert.assertEquals(bodyType, outputMessage.getBodyType());
        Assert.assertEquals(message.getCorrelationId(), outputMessage.getCorrelationId());
        Assert.assertEquals(message.getTransactionId(), outputMessage.getTransactionId());
    }

    private Message constructMessageFromOlderMessage(Message message, PublicationStatus status) {
        Questionnaire questionnaire =
            (Questionnaire) OutcomeDefinitionService.getServerFhirContext().newJsonParser()
                .parseResource(message.getBody());
        questionnaire.setStatus(status);
        Message newMessage = new Message();
        newMessage.setPrefer(Prefer.REPRESENTATION);
        newMessage.setBody(OutcomeDefinitionService.getServerFhirContext().newJsonParser()
            .encodeResourceToString(questionnaire));
        newMessage.setSender("MyEventProcessorMockKafkaTest");
        newMessage.setCorrelationId(message.getCorrelationId());
        newMessage.setTransactionId(message.getTransactionId());
        newMessage.setBodyCategory(BodyCategory.FHIR);
        newMessage.setBodyType("Questionnaire");
        newMessage.setContentVersion(System.getenv("FHIR_VERSION"));
        return newMessage;
    }

    @Test
    public void testCreateSameQuestionnaireTwice() throws Exception {
        //Construct resource string to be used in message
        final String resourceString =
            constructQuestionnaireString("abc", PublicationStatus.ACTIVE, randomIdentifier());
        Message message = constructQuestionnaireMessage(resourceString);

        sendMessage(message, Operation.Create);
        Message outputMessage = receiveMessage(Operation.DataCreated);

        makeAssertions(message, outputMessage, "Questionnaire");
        // Resend same message
        sendMessage(message, Operation.Create);
        Message outputMessageProcessingFailed = receiveMessage(Operation.ProcessingFailed);

        makeAssertions(message, outputMessageProcessingFailed, "OperationOutcome");
    }

    @Test
    public void testUpdateDraftQuestionnaire() throws Exception {
        //Construct resource string to be used in message
        Identifier identifier = randomIdentifier();

        Message message = constructQuestionnaireMessage(
            constructQuestionnaireString("abc", PublicationStatus.DRAFT, identifier));

        sendMessage(message, Operation.Create);
        Message outputMessage = receiveMessage(Operation.DataCreated);

        makeAssertions(message, outputMessage, "Questionnaire");
        // Send same message with update
        sendMessage(constructMessageFromOlderMessage(outputMessage, PublicationStatus.DRAFT),
            Operation.Update);
        Message outputMessageUpdated = receiveMessage(Operation.DataUpdated);
        makeAssertions(message, outputMessageUpdated, "Questionnaire");
    }

    @Test
    public void testUpdateQuestionnaireFromDraftToActive() throws Exception {
        //Construct resource string to be used in message
        Identifier identifier = randomIdentifier();

        Message message = constructQuestionnaireMessage(
            constructQuestionnaireString("abc", PublicationStatus.DRAFT, identifier));

        sendMessage(message, Operation.Create);
        Message outputMessage = receiveMessage(Operation.DataCreated);

        makeAssertions(message, outputMessage, "Questionnaire");
        // Send same message with update
        Message activeMessage =
            constructMessageFromOlderMessage(outputMessage, PublicationStatus.ACTIVE);
        sendMessage(activeMessage, Operation.Update);
        Message outputMessageUpdated = receiveMessage(Operation.DataUpdated);

        makeAssertions(message, outputMessageUpdated, "Questionnaire");
    }

        @Test
        public void testUpdateQuestionnaireFromDraftToActiveWhenAnActiveAlreadyExists()
            throws Exception {
            //Construct resource string to be used in message
            Identifier identifier = randomIdentifier();

            Message draftMessage = constructQuestionnaireMessage(
                constructQuestionnaireString("abc", PublicationStatus.DRAFT, identifier));

            sendMessage(draftMessage, Operation.Create);
            Message outputMessage = receiveMessage(Operation.DataCreated);

            makeAssertions(draftMessage, outputMessage, "Questionnaire");

            TimeUnit.MICROSECONDS.sleep(100);
            // Create same resource but with active status
            Message activeMessage = constructQuestionnaireMessage(
                constructQuestionnaireString("abc", PublicationStatus.ACTIVE, identifier));

            sendMessage(activeMessage, Operation.Create);
            Message outputMessageActive = receiveMessage(Operation.DataCreated);
            makeAssertions(activeMessage, outputMessageActive, "Questionnaire");
            // Send resource with active status, with update
            Message activeUpdateMessage = constructMessageFromOlderMessage(draftMessage,PublicationStatus.ACTIVE);
            sendMessage(activeUpdateMessage, Operation.Update);
            Message outputMessageProcessingFailed = receiveMessage(Operation.ProcessingFailed);

            makeAssertions(draftMessage, outputMessageProcessingFailed, "OperationOutcome");
        }

}
