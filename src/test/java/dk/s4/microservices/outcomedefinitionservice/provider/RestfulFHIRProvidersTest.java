package dk.s4.microservices.outcomedefinitionservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.PreferReturnEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import dk.s4.microservices.outcomedefinitionservice.servlet.ServerFacade;
import org.eclipse.jetty.server.Server;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Task;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;


public class RestfulFHIRProvidersTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(RestfulFHIRProvidersTest.class);
    private static IGenericClient ourClient;
    private static Server ourServer;
    private static String ourServerBase;
    private static long orgId = 1;

    @AfterClass
    public static void afterClass() throws Exception {
        ourServer.stop();
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = RestfulFHIRProvidersTest.class.getClassLoader().getResource(".keep").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        ourLog.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        String ENVIRONMENT_FILE = "service.env";
        TestUtils.readEnvironment(path + "/" + ENVIRONMENT_FILE, environmentVariables);
        TestUtils.readEnvironment(path + "/" + "deploy.env", environmentVariables);


        String value = System.getenv("DATABASE_TYPE");
        if (value == null)
            environmentVariables.set("DATABASE_TYPE", "Derby");

        //Run tests without authentication:
        environmentVariables.set("ENABLE_AUTHENTICATION", "false");
        environmentVariables.set("ENABLE_DIAS_AUTHENTICATION", "false");

        //Init web server and start it
        int ourPort = TestUtils.findFreePort();
        ourServer = new Server(ourPort);
        ourServerBase = TestUtils.initWebServer(path + "/src/main/webapp/WEB-INF/without-keycloak/web.xml", path + "/target/service", environmentVariables, ourPort, ourServer);


        FhirContext fhirContext = ServerFacade.getServerFhirContext();
        ourClient = fhirContext.newRestfulGenericClient(ourServerBase);
        ourClient.registerInterceptor(new LoggingInterceptor(true));

        IParser jsonParser = ServerFacade.getServerFhirContext().newJsonParser();

        String resourceString;

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Questionnaire.json");
        ServerFacade.getQuestionnaireDao().create(jsonParser.parseResource(Questionnaire.class, resourceString));

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/Task.json");
        ServerFacade.getTaskDao().create(jsonParser.parseResource(Task.class, resourceString));

        resourceString = ResourceUtil.stringFromResource("FHIR-clean/ObservationDefinition.json");
        ServerFacade.getObservationDefinitionDao().create(jsonParser.parseResource(ObservationDefinition.class, resourceString));
        ServerFacade.forceReindexing();
    }

    @Test
    public void testQuestionnaireReadNotSupported() {
        try {
            ourClient.read().resource(Questionnaire.class).withId("Questionnaire/" + orgId).execute();
        } catch (NotImplementedOperationException e) {
            Assert.assertNotNull(e);
            return;
        }
        Assert.fail();
    }

    @Test
    public void testQuestionnairePatchNotSupported() {
        NotImplementedOperationException expectedException = null;

        try {
            ourClient.patch().
                    withBody("[ { \"op\":\"replace\", \"path\":\"/title\", \"value\":newTitle } ]").
                    withId("Questionnaire/1").prefer(PreferReturnEnum.OPERATION_OUTCOME).execute();
        } catch (NotImplementedOperationException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testQuestionnaireCreateNotSupported() {
        InvalidRequestException expectedException = null;

        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setName("testQuestionnaireCreate");
        try {
            ourClient.create().resource(questionnaire).execute();
        } catch (InvalidRequestException e) {
            expectedException = e;
        }
        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testQuestionnaireSearch() {
        String searchUrl = ourServerBase + "/Questionnaire?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchWithStatus() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getWithStatus&status=active";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchWithStatusInvalidStatus() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getWithStatus&status=ivalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchWithStatusAndJurisdiction() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getWithStatus&status=active&jurisdiction=urn:oid:1.2.208.176.1.1|6081000016005";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchWithStatusAndJurisdictionInvalidValue() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getWithStatus&status=active&jurisdiction=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchWithStatusAndJurisdictionInvalidSystem() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getWithStatus&status=active&jurisdiction=invalid|6081000016005";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());
    }

    @Test
    public void testQuestionnaireSearchByIdentifier() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|720441d9-727c-4be4-8d2a-8d43838158d4";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertTrue(result.hasEntry());

    }

    @Test
    public void testQuestionnaireSearchByIdentifierInvalidValue() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());

    }

    @Test
    public void testQuestionnaireSearchByIdentifierInvalidSystem() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=invalid|f14997cf-e16e-4e4b-8b3a-1f419386f597";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());

    }

    @Test
    public void testQuestionnaireSearchByIdentifierWithJurisdiction() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|720441d9-727c-4be4-8d2a-8d43838158d4" +
                "&jurisdiction=urn:oid:1.2.208.176.1.1|6081000016005";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertTrue(result.hasEntry());

    }

    @Test
    public void testQuestionnaireSearchByIdentifierWithJurisdictionInvalidValue() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|f14997cf-e16e-4e4b-8b3a-1f419386f597" +
                "&jurisdiction=urn:oid:1.2.208.176.1.1|invalid";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());

    }

    @Test
    public void testQuestionnaireSearchByIdentifierWithJurisdictionInvalidSystem() {
        String searchUrl = ourServerBase + "/Questionnaire?_query=getByIdentifier&identifier=urn:ietf:rfc:3986|f14997cf-e16e-4e4b-8b3a-1f419386f597" +
                "&jurisdiction=invalid|6081000016005";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result;
        result = query.execute();
        Assert.assertFalse(result.hasEntry());

    }

    @Test
    public void testObservationDefinitionReadNotSupported() {
        try {
            ourClient.read().resource(ObservationDefinition.class).withId("ObservationDefinition/" + orgId).execute();
        } catch (NotImplementedOperationException e) {
            Assert.assertNotNull(e);
            return;
        }
        Assert.fail();
    }

    @Test
    public void testObservationDefinitionPatchNotSupported() {
        NotImplementedOperationException expectedException = null;

        try {
            ourClient.patch().
                    withBody("[ { \"op\":\"replace\", \"path\":\"/title\", \"value\":newTitle } ]").
                    withId("ObservationDefinition/1").prefer(PreferReturnEnum.OPERATION_OUTCOME).execute();
        } catch (NotImplementedOperationException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testObservationDefinitionCreateNotSupported() {
        InvalidRequestException expectedException = null;

        ObservationDefinition observationDefinition = new ObservationDefinition();

        try {
            ourClient.create().resource(observationDefinition).execute();
        } catch (InvalidRequestException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
    }

    @Test
    public void testObservationDefinitionSearch() {
        String searchUrl = ourServerBase + "/ObservationDefinition?";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testObservationDefinitionSearchByIdentifier() {
        String searchUrl = ourServerBase + "/ObservationDefinition?_identifier=urn:ietf:rfc:3986|92a67ed7-d89a-4cd9-b546-ee277ab76fb9";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetForPatientWithCode() {
        String searchUrl = ourServerBase + "/Task?_query=getForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2Btype%2Bcoding%2Bsystem" +
                "|acknowledgement";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());

        // Try with authored_on param

        searchUrl = searchUrl + "&authored-on=ge2016-01-01&authored-on=le2019-01-01";

        query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }

    @Test
    public void testGetForPatientWithCodeAndOwner() {
        String searchUrl = ourServerBase + "/Task?_query=getForPatientWithCode" +
                "&identifier=urn:oid:1.2.208.176.1.2|2512489996" +
                "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2Btype%2Bcoding%2Bsystem" +
                "|acknowledgement" +
                "&owner=urn:oid:1.2.208.176.1.1|453191000016003";
        IQuery<Bundle> query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        Bundle result = query.execute();

        Assert.assertTrue(result.hasEntry());

        // Try with authored_on param

        searchUrl = searchUrl + "&authored-on=ge2016-01-01&authored-on=le2019-01-01";

        query = ourClient.search().byUrl(searchUrl).returnBundle(Bundle.class);

        result = query.execute();

        Assert.assertTrue(result.hasEntry());
    }
}
