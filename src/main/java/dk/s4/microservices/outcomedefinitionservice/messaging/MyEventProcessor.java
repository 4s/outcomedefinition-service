package dk.s4.microservices.outcomedefinitionservice.messaging;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.microservicecommon.fhir.FhirCUDEventProcessor;
import java.util.List;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Enumerations.PublicationStatus;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor extends FhirCUDEventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);
    private final IFhirResourceDao<ObservationDefinition> observationDefinitionDAOR4;
    private final IFhirResourceDao<Questionnaire> questionnaireDaoR4;
    private final IFhirResourceDao<Task> taskDaoR4;
    private final FhirContext fhirContext;

    public MyEventProcessor(FhirContext fhirContext,
        IFhirResourceDao<ObservationDefinition> observationDefinitionDAOR4,
        IFhirResourceDao<Questionnaire> questionnaireDaoR4, IFhirResourceDao<Task> taskDaoR4) {
        super(fhirContext);
        this.fhirContext = fhirContext;
        this.observationDefinitionDAOR4 = observationDefinitionDAOR4;
        this.questionnaireDaoR4 = questionnaireDaoR4;
        this.taskDaoR4 = taskDaoR4;
    }

    @Override
    protected IFhirResourceDao daoForResourceType(String resourceType) {
        logger.debug("daoForResourceType: Type is: " + resourceType);

        if (resourceType.equals("Questionnaire")) {
            return questionnaireDaoR4;
        }
        if (resourceType.equals("ObservationDefinition")) {
            return observationDefinitionDAOR4;
        }
        if (resourceType.equals("Task")) {
            return taskDaoR4;
        }

        logger.error("No DAO for resource type - should not happen");
        return null;
    }

    @Override
    protected boolean processCreateOrUpdate(Topic consumedTopic, Message receivedMessage,
        Topic messageProcessedTopic, Message outgoingMessage) {
        if(resourceCollides(consumedTopic,receivedMessage)){
            throw new InvalidRequestException(
                "Trying to update resource but a resource with same status, identifier and version already exists. " + receivedMessage);
        }
        return super.processCreateOrUpdate(consumedTopic, receivedMessage, messageProcessedTopic,
            outgoingMessage);
    }

    /**
     * Checks whether the update/create request is allowed.
     * If the resource is a questionnaire, we must ensure that no other questionnaires exist,
     * that has the same version, identifier and status. However this is allowed, if we are dealing with
     * an update request, and said resource has the status 'draft' (we allow updating questionnaires that have not
     * been activated yet).
     * @param consumedTopic Topic describing which type of request is sent.
     * @param receivedMessage The message containing the resource.
     * @return True if request is not allowed (There is a collision), false otherwise.
     */
    protected boolean resourceCollides(Topic consumedTopic, Message receivedMessage){
        if (receivedMessage.getBodyType().equals("Questionnaire")) {
            Questionnaire newQuestionnaire = parseToQuestionnaire(receivedMessage.getBody());
            if ((consumedTopic.getOperation() == Topic.Operation.Update
                && newQuestionnaire.getStatus() == PublicationStatus.ACTIVE)
                || consumedTopic.getOperation() == Operation.Create) {
                IBundleProvider questionnaireBundleProvider =
                    searchForCollidingQuestionnaires(newQuestionnaire);
                return questionnaireBundleProvider.size() > 0;
            }
        }
        return false;
    }

    private IBundleProvider searchForCollidingQuestionnaires(Questionnaire questionnaire) {
        Identifier identifier = findIdentifierWithCorrectSystem(questionnaire.getIdentifier());
        SearchParameterMap paramMap = new SearchParameterMap();
        paramMap.add("version", new TokenParam().setValue(questionnaire.getVersion()));
        paramMap.add(Questionnaire.SP_IDENTIFIER,
            new TokenParam().setValue(identifier.getValue()).setSystem(identifier.getSystem()));
        paramMap.add(Questionnaire.SP_STATUS,
            new TokenParam().setValue(questionnaire.getStatus().toCode()));
        return questionnaireDaoR4.search(paramMap);
    }

    private Identifier findIdentifierWithCorrectSystem(List<Identifier> identifierList) {
        for (Identifier identifier : identifierList) {
            if (identifier.getSystem()
                .equals(System.getenv("OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM"))) {
                return identifier;
            }
        }
        throw new InvalidRequestException(
            "The resource provided did not contain an identifier with the correct system");
    }

    private Questionnaire parseToQuestionnaire(String questionnaireBody) {
        return (Questionnaire) fhirContext.newJsonParser().parseResource(questionnaireBody);
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        Identifier identifier = null;
        if (resource instanceof Questionnaire) {
            Questionnaire questionnaire = (Questionnaire) resource;
            identifier = questionnaire.getIdentifier().stream().filter(ident -> ident.getSystem()
                .equals(System.getenv("OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM"))).findAny()
                .orElse(null);
        } else if (resource instanceof ObservationDefinition) {
            ObservationDefinition obsDef = (ObservationDefinition) resource;
            identifier = obsDef.getIdentifier().stream().filter(ident -> ident.getSystem()
                .equals(System.getenv("OFFICIAL_OBSERVATION_DEFINITION_IDENTIFIER_SYSTEM")))
                .findAny().orElse(null);
        } else if (resource instanceof Task) {
            Task task = (Task) resource;
            identifier = task.getIdentifier().stream().filter(ident -> ident.getSystem()
                .equals(System.getenv("OFFICIAL_TASK_DEFINITION_IDENTIFIER_SYSTEM"))).findAny()
                .orElse(null);
        }

        return identifier;
    }
}
