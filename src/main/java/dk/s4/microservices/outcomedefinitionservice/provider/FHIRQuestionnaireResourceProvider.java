package dk.s4.microservices.outcomedefinitionservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FHIRQuestionnaireResourceProvider extends BaseJpaResourceProvider<Questionnaire> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProvider.class);

    public FHIRQuestionnaireResourceProvider(IFhirResourceDao<Questionnaire> dao, FhirContext fhirContext) {
        super(dao);
        setContext(fhirContext);
    }


    /**
     * The getResourceType method comes from IResourceProvider, and must be
     * overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<Questionnaire> getResourceType() {
        return Questionnaire.class;
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
                                  RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Questionnaire read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }

    /**
     * Retrieves all Questionnaires with a given status
     *
     * @param theRequest
     * @param theResponse
     * @param theStatus
     * @return bundle containing Questionnaires
     */

    @Search(queryName = "getWithStatus")
    public Bundle getWithStatus(HttpServletRequest theRequest,
                                            HttpServletResponse theResponse,
                                            RequestDetails theDetails,
                                            @Description(shortDefinition="The current status of the questionnaire")
                                            @OptionalParam(name="status")
                                                  TokenAndListParam theStatus,
                                            @Description(shortDefinition="Intended jurisdiction for the questionnaire")
                                            @OptionalParam(name="jurisdiction") TokenOrListParam theJurisdiction) {
        logger.debug("getWithStatus");

        SearchParameterMap paramMap = new SearchParameterMap();

        paramMap.add(Questionnaire.SP_STATUS,theStatus);
        paramMap.add(Questionnaire.SP_JURISDICTION, theJurisdiction);

        IBundleProvider carePlanBundleProvider = getDao().search(paramMap,theDetails,theResponse);

        Bundle retVal = new Bundle();
        List<IBaseResource> questionnaires = carePlanBundleProvider.getResources(0,carePlanBundleProvider.size());
        for (IBaseResource questionnaire : questionnaires){
            String classString = questionnaire.getClass().getName();
            if(classString.equals("org.hl7.fhir.r4.model.Questionnaire")){
                retVal.addEntry().setResource((Questionnaire) questionnaire);
            }
        }
        return retVal;
    }

    /**
     *  Returns a bundle of all Questionnaires that match the given identifier.
     *
     * @param theRequest
     * @param theResponse
     * @param theDetails
     * @param theIdentifier
     * @return
     */

    @Search(queryName = "getByIdentifier")
    public IBundleProvider getByIdentifier (HttpServletRequest theRequest,
                                                HttpServletResponse theResponse,
                                                RequestDetails theDetails,
                                                @Description(shortDefinition="External identifier for the questionnaire")
                                                @RequiredParam(name=Questionnaire.SP_IDENTIFIER) TokenParam theIdentifier,
                                                @Description(shortDefinition="Intended jurisdiction for the questionnaire")
                                                @OptionalParam(name=Questionnaire.SP_JURISDICTION) TokenOrListParam theJurisdiction) {
        logger.debug("getByIdentifier");

        SearchParameterMap paramMap = new SearchParameterMap();
        paramMap.add(Questionnaire.SP_IDENTIFIER,theIdentifier);
        paramMap.add(Questionnaire.SP_JURISDICTION, theJurisdiction);

        return getDao().search(paramMap,theDetails,theResponse);
    }


    @Search(allowUnknownParams=true)
    public ca.uhn.fhir.rest.api.server.IBundleProvider search(
            javax.servlet.http.HttpServletRequest theServletRequest,
            javax.servlet.http.HttpServletResponse theServletResponse,
            ca.uhn.fhir.rest.api.server.RequestDetails theRequestDetails,
            @Description(shortDefinition="The ID of the resource")
            @OptionalParam(name="_id")
                    TokenAndListParam the_id,
            @Description(shortDefinition="The questionnaire publication date")
            @OptionalParam(name="date")
                    DateRangeParam theDate,
            @Description(shortDefinition="External identifier for the questionnaire")
            @OptionalParam(name=Questionnaire.SP_IDENTIFIER)
                    TokenAndListParam theIdentifier,
            @Description(shortDefinition="Intended jurisdiction for the questionnaire")
            @OptionalParam(name=Questionnaire.SP_JURISDICTION)
                    TokenOrListParam theJurisdiction,
            @Description(shortDefinition="Computationally friendly name of the questionnaire")
            @OptionalParam(name="name")
                    StringAndListParam theName,
            @Description(shortDefinition="The current status of the questionnaire")
            @OptionalParam(name=Questionnaire.SP_STATUS)
                    TokenAndListParam theStatus,
            @Description(shortDefinition="Resource that can be subject of QuestionnaireResponse")
            @OptionalParam(name="subject-type")
                    TokenAndListParam theSubject_type,
            @Description(shortDefinition="The human-friendly name of the questionnaire")
            @OptionalParam(name="title")
                    StringAndListParam theTitle,
            @Description(shortDefinition="The uri that identifies the questionnaire")
            @OptionalParam(name="url")
                    UriAndListParam theUrl,
            @Description(shortDefinition="The business version of the questionnaire")
            @OptionalParam(name="version")
                    TokenAndListParam theVersion,
            @RawParam
                    Map<String, List<String>> theAdditionalRawParams,
            @IncludeParam(reverse=true)
                    Set<Include> theRevIncludes,
            @Description(shortDefinition="Only return resources which were last updated as specified by the given range")
            @OptionalParam(name="_lastUpdated")
                    DateRangeParam theLastUpdated,
            @IncludeParam(allow= {
                    "*"
            })
                    Set<Include> theIncludes,
            @Sort
                    SortSpec theSort,
            @ca.uhn.fhir.rest.annotation.Count
                    Integer theCount,
            SummaryEnum theSummaryMode,
            SearchTotalModeEnum theSearchTotalMode
    ) {
        startRequest(theServletRequest);
        try {
            SearchParameterMap paramMap = new SearchParameterMap();
            paramMap.add(Questionnaire.SP_IDENTIFIER, theIdentifier);
            paramMap.add(Questionnaire.SP_JURISDICTION, theJurisdiction);
            paramMap.add(Questionnaire.SP_NAME, theName);
            paramMap.add(Questionnaire.SP_STATUS, theStatus);
            paramMap.add(Questionnaire.SP_SUBJECT_TYPE, theSubject_type);
            paramMap.add(Questionnaire.SP_TITLE, theTitle);
            paramMap.add(Questionnaire.SP_URL, theUrl);
            paramMap.add(Questionnaire.SP_VERSION, theVersion);
            paramMap.setRevIncludes(theRevIncludes);
            paramMap.setLastUpdated(theLastUpdated);
            paramMap.setIncludes(theIncludes);
            paramMap.setSort(theSort);
            paramMap.setCount(theCount);
            paramMap.setSummaryMode(theSummaryMode);
            paramMap.setSearchTotalMode(theSearchTotalMode);

            getDao().translateRawParameters(theAdditionalRawParams, paramMap);

            ca.uhn.fhir.rest.api.server.IBundleProvider retVal = getDao().search(paramMap, theRequestDetails, theServletResponse);
            return retVal;
        } finally {
            endRequest(theServletRequest);
        }
    }

}
