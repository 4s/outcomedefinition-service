package dk.s4.microservices.outcomedefinitionservice.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.DaoMethodOutcome;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.provider.BaseJpaResourceProvider;
import ca.uhn.fhir.jpa.searchparam.SearchParameterMap;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.PatchTypeEnum;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.outcomedefinitionservice.Utils.CustomSearchParameter;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FHIRTaskResourceProvider extends BaseJpaResourceProvider<Task> {

    private static final Logger logger = LoggerFactory.getLogger(FHIRTaskResourceProvider.class);

    public FHIRTaskResourceProvider(IFhirResourceDao<Task> dao, FhirContext fhirContext) {
        super(dao);
        setContext(fhirContext);
    }

    /**
     * The getResourceType method comes from IResourceProvider, and must be
     * overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<Task> getResourceType() {
        return Task.class;
    }

    /**
     * We don't want to support patch operation via REST. We thus override patch defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Patch
    public DaoMethodOutcome patch(HttpServletRequest theRequest, @IdParam IIdType theId,
                                  RequestDetails theRequestDetails, @ResourceParam String theBody, PatchTypeEnum thePatchType) {
        throw new NotImplementedOperationException("RESTful patch operation is not supported by this FHIR server");
    }

    /**
     * We don't want to support read operation via REST. We thus override read defined in BaseJpaResourceProvider and
     * always throw a NotImplementedOperationException.
     */
    @Override
    @Read
    public Task read(HttpServletRequest theRequest, @IdParam IIdType theId, RequestDetails theRequestDetails) {
        throw new NotImplementedOperationException("RESTful read operation is not supported by this FHIR server");
    }


    /**
     * Returns a bundle of tasks for a given patient, given code and within a given period (optional).
     *
     * @param theRequest the http request
     * @param theResponse the http response
     * @param theRequestDetail the http request details
     * @param code the FHIR Task code, either acknowledgement, review or activity
     * @param identifier the identifier of the patient
     * @param authored_on the period
     * @return Bundle containing tasks
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task+type+coding+system">Task type coding system</a>
     */

    @Search(queryName = "getForPatientWithCode")
    public IBundleProvider getForPatientWithCode(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                                 RequestDetails theRequestDetail,
                                                 @RequiredParam(name=Task.SP_CODE) TokenParam code,
                                                 @RequiredParam(name=Patient.SP_IDENTIFIER) TokenParam identifier,
                                                 @OptionalParam(name=Task.SP_OWNER) TokenOrListParam owner,
                                                 @OptionalParam(name=Task.SP_AUTHORED_ON) DateRangeParam authored_on){
        logger.debug("getForPatientWithCode");

        SearchParameterMap parameterMap = new SearchParameterMap();
        parameterMap.add(CustomSearchParameter.PATIENT_IDENTIFIER.getParameter(), identifier);
        parameterMap.add(Task.SP_CODE, code);
        parameterMap.add(CustomSearchParameter.OWNER_IDENTIFIER.getParameter(), owner);
        parameterMap.add(Task.SP_AUTHORED_ON, authored_on);

        return getDao().search(parameterMap,theRequestDetail,theResponse);
    }

}

