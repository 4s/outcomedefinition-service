package dk.s4.microservices.outcomedefinitionservice.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDao;
import ca.uhn.fhir.jpa.search.reindex.IResourceReindexingSvc;
import ca.uhn.fhir.jpa.searchparam.registry.ISearchParamRegistry;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.server.IResourceProvider;
import dk.s4.microservices.genericresourceservice.servlet.JpaServerGenericServlet;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.Env;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.MetricsEventConsumerInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.SearchParameterFacade;
import dk.s4.microservices.microservicecommon.security.*;
import dk.s4.microservices.outcomedefinitionservice.health.HealthEndpoint;
import dk.s4.microservices.outcomedefinitionservice.messaging.KafkaInterceptorAdaptor;
import dk.s4.microservices.outcomedefinitionservice.messaging.MyEventProcessor;
import dk.s4.microservices.outcomedefinitionservice.provider.FHIRObservationDefinitionResourceProvider;
import dk.s4.microservices.outcomedefinitionservice.provider.FHIRQuestionnaireResourceProvider;
import dk.s4.microservices.outcomedefinitionservice.provider.FHIRTaskResourceProvider;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.SearchParameter;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.getResourceStream;

/**
 * OutcomeDefinition Service.
 * <p>
 * It exposes a RESTful interface for FHIR Questionnaire and Observation Definition resources.
 */
public class OutcomeDefinitionService extends JpaServerGenericServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(OutcomeDefinitionService.class);

    private static FhirContext fhirContext;
    private static IFhirResourceDao<ObservationDefinition> observationDefinitionDAOR4;
    private static IFhirResourceDao<Questionnaire> questionnaireDaoR4;
    private static IFhirResourceDao<Task> taskDaoR4;
    private static MyEventProcessor eventProcessor;
    private static List<Topic> topics;
    private static IResourceReindexingSvc reindexing;
    private KafkaConsumeAndProcess kafkaConsumeAndProcess;
    private Thread kafkaConsumeAndProcessThread;

    private UserContextResolverInterface userResolver;

    public OutcomeDefinitionService() {
        super(getMyFhirContext(), logger);
    }

    /**
     * Singleton FhirContext
     *
     * @return the FhirContext
     */
    private static FhirContext getMyFhirContext() {
        if (fhirContext == null) {
            fhirContext = FhirContext.forR4();
            fhirContext.setParserErrorHandler(new StrictErrorHandler());
        }
        return fhirContext;
    }

    @Override
    public UserContextResolverInterface getUserResolver() {
        if(userResolver == null) {
            if (System.getenv("ENABLE_DIAS_AUTHENTICATION").equals("true")) {
                userResolver = new DiasUserContextResolver(System.getenv("USER_CONTEXT_SERVICE_URL"));
            } else if (System.getenv("ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION").equals("true")) {
                userResolver = new KeycloakGatekeeperUserContextResolver();
            } else if (System.getenv("ENABLE_OAUTH2_PROXY_AUTHORIZATION").equals("true")) {
                userResolver = new OAuth2ProxyUserContextResolver();
            } else {
                userResolver = new DefaultUserContextResolver();
            }
        }
        return userResolver;
    }

    @Override
    public void destroy() {
        System.out.println("Shutting down OutcomeDefinitionService");
        if(kafkaEnabled()) {
            try {
                kafkaConsumeAndProcess.stopThread();
                kafkaConsumeAndProcessThread.join(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void registerAndCheckEnvironmentVars() {
        Env.registerRequiredEnvVars(Arrays.asList(
            "SERVICE_NAME",
            "FHIR_VERSION",
            "CORRELATION_ID",
            "TRANSACTION_ID",
            "LOG_LEVEL",
            "LOG_LEVEL_DK_S4",
            "ENABLE_KAFKA",
            "SERVER_BASE_URL",
            "DATABASE_URL",
            "DATABASE_USERNAME",
            "DATABASE_PASSWORD",
            "INITIALIZE_SEARCH_PARAM",
            "OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM",
            "OFFICIAL_OBSERVATION_DEFINITION_IDENTIFIER_SYSTEM",
            "OFFICIAL_TASK_DEFINITION_IDENTIFIER_SYSTEM"
    ));
        Env.registerConditionalEnvVars("ENABLE_KAFKA",
                Arrays.asList(
                        "KAFKA_BOOTSTRAP_SERVER",
                        "KAFKA_KEY_DESERIALIZER",
                        "KAFKA_VALUE_DESERIALIZER",
                        "KAFKA_ENABLE_AUTO_COMMIT",
                        "KAFKA_AUTO_COMMIT_INTERVAL_MS",
                        "KAFKA_SESSION_TIMEOUT_MS",
                        "KAFKA_GROUP_ID",
                        "KAFKA_ACKS",
                        "KAFKA_RETRIES",
                        "KAFKA_KEY_SERIALIZER",
                        "KAFKA_VALUE_SERIALIZER",
                        "ENABLE_HEALTH",
                        "HEALTH_FILE_PATH",
                        "HEALTH_INTERVAL_MS")
        );
    }

    @Override
    protected void initialize() throws ServletException {
        super.initialize();
        registerAndCheckEnvironmentVars();

        logger.debug("initialize");

        // Get the spring context from the web container (it's declared in web.xml)
        WebApplicationContext myAppCtx = ContextLoaderListener.getCurrentWebApplicationContext();

        questionnaireDaoR4 = myAppCtx.getBean("myQuestionnaireDaoR4", IFhirResourceDao.class);
        observationDefinitionDAOR4 = myAppCtx.getBean("myObservationDefinitionDaoR4", IFhirResourceDao.class);
        taskDaoR4 = myAppCtx.getBean("myTaskDaoR4", IFhirResourceDao.class);
        IFhirResourceDao<SearchParameter> searchParameterDaoR4 = myAppCtx.getBean("mySearchParameterDaoR4", IFhirResourceDao.class);
        reindexing = myAppCtx.getBean(IResourceReindexingSvc.class);


        //Initiate Custom SearchParameters
        SearchParameterFacade searchParameterFacade = new SearchParameterFacade(searchParameterDaoR4,getMyFhirContext());
        try {
            searchParameterFacade.installSearchParameter(getResourceStream("TaskPatientSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("TaskOwnerSearchParam.json"));
            searchParameterFacade.installSearchParameter(getResourceStream("ObservationDefinitionIdentifierSearchParam.json"));
            myAppCtx.getBean(ISearchParamRegistry.class).forceRefresh();
        } catch (IOException e) {
            throw new InternalError("Something went wrong while reading the custom SearchParameters");
        }

        List<IResourceProvider> resourceProviders = new ArrayList<>();
        resourceProviders.add(new FHIRObservationDefinitionResourceProvider(observationDefinitionDAOR4, getMyFhirContext()));
        resourceProviders.add(new FHIRQuestionnaireResourceProvider(questionnaireDaoR4, getMyFhirContext()));
        resourceProviders.add(new FHIRTaskResourceProvider(taskDaoR4, getMyFhirContext()));

        setResourceProviders(resourceProviders);

        topics = Arrays.asList(
                FhirTopics.create("ObservationDefinition"),
                FhirTopics.update("ObservationDefinition"),

                FhirTopics.create("Questionnaire"),
                FhirTopics.update("Questionnaire"),

                FhirTopics.create("Task"),
                FhirTopics.update("Task")
        );

        eventProcessor = new MyEventProcessor(getMyFhirContext(), observationDefinitionDAOR4, questionnaireDaoR4, taskDaoR4);
        if (kafkaEnabled()) {
            try {
                KafkaEventProducer kafkaEventProducer = new KafkaEventProducer(System.getenv("SERVICE_NAME"));
                kafkaConsumeAndProcess = new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor);
                kafkaConsumeAndProcess.registerInterceptor(new MetricsEventConsumerInterceptorAdaptor());
                kafkaConsumeAndProcess.registerInterceptor(new KafkaInterceptorAdaptor(getMyFhirContext()));
                if (Env.isSetToTrue("ENABLE_DIAS_AUTHENTICATION")) {
                    kafkaConsumeAndProcess.registerInterceptor(new DiasEventConsumerInterceptor());
                }
                kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
                kafkaConsumeAndProcessThread.start();
                HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);
            } catch (KafkaInitializationException | MessagingInitializationException e) {
                logger.error("Error during Kafka initialization: " + e.getMessage(), e);
                System.exit(0);
            }
        }
    }

    private boolean kafkaEnabled(){
        String enableKafkaString = System.getenv("ENABLE_KAFKA");
        logger.debug("Read ENABLE_KAFKA from environment: " + enableKafkaString);
        return (enableKafkaString != null && enableKafkaString.equals("true"));
    }

    /**
     * For testing
     */
    static FhirContext getServerFhirContext() {
        return getMyFhirContext();
    }

    /**
     * For testing
     */
    static void forceReindexing() {
        if (reindexing != null) {
            reindexing.forceReindexingPass();
        }
        else {
            logger.error("Reindexing failed");
        }
    }

    /**
     * For testing
     */
    static IFhirResourceDao<ObservationDefinition> getObservationDefinitionDao() {
        if (observationDefinitionDAOR4 != null) {
            return observationDefinitionDAOR4;
        }
        throw new RuntimeException("ObservationDefinition dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Questionnaire> getQuestionnaireDao() {
        if (questionnaireDaoR4 != null) {
            return questionnaireDaoR4;
        }
        throw new RuntimeException("Questionnaire dao not initialized");
    }

    /**
     * For testing
     */
    static IFhirResourceDao<Task> getTaskDao() {
        if (taskDaoR4 != null) {
            return taskDaoR4;
        }
        throw new RuntimeException("Task dao not initialized");
    }

    /**
     * For testing
     */
    static MyEventProcessor getEventProcessor() {
        if (eventProcessor != null) {
            return eventProcessor;
        }
        throw new RuntimeException("Event processor not initialized");
    }

    /**
     * For testing
     */
    static List<Topic> getTopics() {
        if (topics != null) {
            return topics;
        }
        throw new RuntimeException("Topics not initialized");
    }
}
