package dk.s4.microservices.outcomedefinitionservice.Utils;

public enum CustomSearchParameter {
    PATIENT_IDENTIFIER("patient-identifier"),
    OWNER_IDENTIFIER("owner-identifier"),
    OBSERVATION_IDENTIFIER("observation-definition-identifier");

    private String parameter;

    CustomSearchParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}
