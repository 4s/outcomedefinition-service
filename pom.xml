<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>dk.s4.microservices</groupId>
		<artifactId>fhir-resource-service-common</artifactId>
		<version>9.0.0</version>
	</parent>

	<artifactId>outcomedefinition-service</artifactId>
	<version>4.2.2</version>
	<packaging>war</packaging>


    <properties>
        <microservice-common-version>7.3.3</microservice-common-version>
        <messaging-version>14.2.3</messaging-version>
        <webResourceDir>src/main/webapp/WEB-INF/without-keycloak</webResourceDir>
        <generic-resource-service>6.0.6</generic-resource-service>
        <sample-data>3.0.1</sample-data>
    </properties>

	<name>OutcomeDefinitionService</name>

    <repositories>
        <repository>
            <id>alexandranexus</id>
            <name>Alexandra RELEASES</name>
            <url>https://maven.alexandra.dk/repository/maven-releases</url>
        </repository>
    </repositories>
	<dependencies>
        <dependency>
            <groupId>dk.s4.microservices</groupId>
            <artifactId>messaging</artifactId>
            <version>${messaging-version}</version>
        </dependency>

        <dependency>
            <groupId>dk.s4.microservices</groupId>
            <artifactId>microservice-common</artifactId>
            <version>${microservice-common-version}</version>
        </dependency>
        <dependency>
            <groupId>dk.s4.microservices</groupId>
            <artifactId>generic-resource-service</artifactId>
            <version>${generic-resource-service}</version>
        </dependency>

        <dependency>
            <groupId>dk.s4.sampledata</groupId>
            <artifactId>sample-data</artifactId>
            <version>${sample-data}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
          <groupId>com.squareup.okhttp3</groupId>
          <artifactId>okhttp</artifactId>
          <version>3.11.0</version>
        </dependency>

        <!-- Prometheus -->
        <!-- The client -->
        <dependency>
          <groupId>io.prometheus</groupId>
          <artifactId>simpleclient</artifactId>
          <version>0.6.0</version>
        </dependency>
        <!-- Hotspot JVM metrics-->
        <dependency>
          <groupId>io.prometheus</groupId>
          <artifactId>simpleclient_hotspot</artifactId>
          <version>0.6.0</version>
        </dependency>
        <!-- Exposition servlet-->
        <dependency>
          <groupId>io.prometheus</groupId>
          <artifactId>simpleclient_servlet</artifactId>
          <version>0.0.26</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.1.3.RELEASE</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.200</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>withKeycloak</id>
            <properties>
                <!-- Default folder with web.xml and jetty-web.xml *with* Keycloak integration -->
                <webResourceDir>src/main/webapp/WEB-INF/with-keycloak</webResourceDir>
            </properties>
        </profile>
    </profiles>

    <build>

        <!-- Tells Maven to name the generated WAR file as service.war -->
        <finalName>service</finalName>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <source>8</source>
                    <excludePackageNames>dk.s4.microservices.fhir-resource-service-common</excludePackageNames>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-web-resource-files</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target name="copy resource files to WEB-INF">
                                <copy todir="src/main/webapp/WEB-INF" overwrite="true">
                                    <fileset dir="${webResourceDir}">
                                        <include name="**/*"/>
                                    </fileset>
                                </copy>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- The configuration here tells the WAR plugin to include the FHIR Tester overlay. You can omit it if you are not using that feature. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <webResources>
                        <resource>
                            <directory>${webResourceDir}</directory>
                        </resource>
                    </webResources>
                    <archive>
                        <manifestEntries>
                            <Build-Time>${maven.build.timestamp}</Build-Time>
                        </manifestEntries>
                    </archive>
                    <overlays>
                        <overlay>
                            <groupId>ca.uhn.hapi.fhir</groupId>
                            <artifactId>hapi-fhir-testpage-overlay</artifactId>
                        </overlay>
                    </overlays>
                    <webXml>${webResourceDir}/web.xml</webXml>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <configuration>
                    <webXml>${webResourceDir}/web.xml</webXml>
                    <jettyXml>${webResourceDir}/jetty-web.xml</jettyXml>
                </configuration>
            </plugin>
            <plugin>
                <!-- Makes unittest execute sequentially -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <forkCount>1</forkCount>
                    <reuseForks>false</reuseForks>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
